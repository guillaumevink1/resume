My pagedown rendered CV
-------------------------------------------------------------
This repo contains the source-code and results of my CV built with the [pagedown](https://github.com/rstudio/pagedown) package and a modified version of the 'resume' templates provided by [Nick Strayer](https://github.com/nstrayer/cv) and [Christoph Scheuch](https://github.com/christophscheuch/cv).
